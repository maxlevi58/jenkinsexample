#!/usr/bin/env python3
import sys
import datetime

from argparse import ArgumentParser

def printInfo(args):
    print(f"name: {args.name}")
    if args.year:
        print(f"year of birth: {args.year}")
        print("Calculating age...")
        print(f"your age is: {datetime.datetime.now().year - int(args.year)}")

def main():
    parser = ArgumentParser()
    parser.add_argument('name')
    parser.add_argument('--year', help='Year of birth')
    args = parser.parse_args()
    printInfo(args)
    return 0


if __name__ == '__main__':
    sys.exit(main())